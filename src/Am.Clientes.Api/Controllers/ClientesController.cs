﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Application.Interfaces;
using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Am.Clientes.Api.Controllers
{
    public class ClientesController : ApiController
    {
        //DI
        private readonly IClienteAppService _cliente;
        public ClientesController(IClienteAppService cliente)
        {
            _cliente = cliente;
        }


        // GET: api/Clientes
        /// <summary>
        /// Retorna Uma lista com os Clientes cadastrados.
        /// </summary>
        /// <returns>Json Clientes</returns>
        public IEnumerable<ClienteDTO> Get()
        {
            return _cliente.Get().AsEnumerable();

        }

        // GET: api/Clientes/5
        /// <summary>
        /// Retorna um cliente pelo id
        /// </summary>
        /// <param name="id">Id do Cliente</param>
        /// <returns>Json Cliente</returns>
        public IHttpActionResult Get(int id)
        {
            try
            {
                ClienteDTO cliente = _cliente.Get(id);
                if (cliente == null)
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Cliente não localizado."));

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, cliente));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));

            }
        }

        // POST: api/Clientes
        /// <summary>
        /// Cadastra um novo Cliente
        /// </summary>
        /// <param name="cliente">Json Cliente</param>
        /// <returns>Json Cliente</returns>
        public IHttpActionResult Post(ClienteDTO cliente)
        {
            try
            {               
                ClienteDTO _client = _cliente.AdicionarCliente(cliente);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, _client));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));
            }
        }

        // PUT: api/Clientes/5
        /// <summary>
        /// Edita os dados de um cliente pelo id
        /// </summary>
        /// <param name="id">Id do Cliente</param>
        /// <param name="cliente">Json Cliente</param>
        /// <returns>Json Cliente</returns>
        public IHttpActionResult Put(int id, [FromBody]ClienteDTO cliente)
        {
            try
            {
                //retorna o cliente baseado no id informado
                ClienteDTO clienteAtual = _cliente.Get(id);
                if (clienteAtual == null)
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Cliente não localizado."));
                //realiza as alterações no objeto
                clienteAtual.Nome = cliente.Nome;
                clienteAtual.Cpf = cliente.Cpf;
                clienteAtual.DataNascimento = cliente.DataNascimento;
                clienteAtual.Endereco = new EnderecoDTO
                {
                    Id = clienteAtual.Endereco.Id,
                    Logradouro = cliente.Endereco.Logradouro,
                    Numero = cliente.Endereco.Numero,
                    Complemento = cliente.Endereco.Complemento,
                    Bairro = cliente.Endereco.Bairro,
                    Cidade = cliente.Endereco.Cidade,
                    Estado = cliente.Endereco.Estado
                };

                ClienteDTO _client = _cliente.EditarCliente(clienteAtual);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, _client));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));
            }
        }

        // DELETE: api/Clientes/5
        /// <summary>
        /// Remove um cliente pelo id
        /// </summary>
        /// <param name="id">Id do Cliente</param>
        /// <returns></returns>
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _cliente.Remove(id);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, "Cliente removido com sucesso."));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));

            }
        }
    }
}
