﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Am.Clientes.Api.Controllers
{
    public class EnderecosController : ApiController
    {
        //DI
        private readonly IEnderecoAppService _endereco;
        public EnderecosController(IEnderecoAppService endereco)
        {
            _endereco = endereco;
        }


        // GET: api/Clientes
        /// <summary>
        /// Retorna Uma lista com os Endereços cadastrados.
        /// </summary>
        /// <returns>Json Endereços</returns>
        public IEnumerable<EnderecoDTO> Get()
        {
            return _endereco.Get().AsEnumerable();

        }

        // GET: api/Clientes/5
        /// <summary>
        /// Retorna um Endereço pelo id
        /// </summary>
        /// <param name="id">Id do Endereço</param>
        /// <returns>Json Endereço</returns>
        public IHttpActionResult Get(int id)
        {
            try
            {
                EnderecoDTO endereco = _endereco.Get(id);
                if (endereco == null)
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Endereço não localizado."));

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, endereco));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));

            }
        }

        // POST: api/Clientes
        /// <summary>
        /// Cadastra um novo Endereço
        /// </summary>
        /// <param name="endereco">Json Endereço</param>
        /// <returns>Json Endereço</returns>
        public IHttpActionResult Post(EnderecoDTO endereco)
        {
            try
            {
                EnderecoDTO _end = _endereco.AdicionarEndereco(endereco);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, _end));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));

            }
        }

        // PUT: api/Clientes/5
        /// <summary>
        /// Edita os dados do Endereço
        /// </summary>
        /// <param name="id">Id do Endereço</param>
        /// <param name="endereco">Json Endereço</param>
        /// <returns>Json Endereço</returns>
        public IHttpActionResult Put(int id, [FromBody]EnderecoDTO endereco)
        {
            try
            {
                //retorna o cliente baseado no id informado
                EnderecoDTO enderecoAtual = _endereco.Get(id);
                if (enderecoAtual == null)
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Endereço não localizado."));
                //realiza as alterações no objeto

                enderecoAtual.Logradouro = endereco.Logradouro;
                enderecoAtual.Numero = endereco.Numero;
                enderecoAtual.Complemento = endereco.Complemento;
                enderecoAtual.Bairro = endereco.Bairro;
                enderecoAtual.Cidade = endereco.Cidade;
                enderecoAtual.Estado = endereco.Estado;
                

                EnderecoDTO _end = _endereco.EditarEndereco(enderecoAtual);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, _end));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));
            }
        }

        // DELETE: api/Clientes/5
        /// <summary>
        /// Remove um endereço
        /// </summary>
        /// <param name="id">Id do Endereço</param>
        /// <returns></returns>
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _endereco.Remove(id);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, "Endereco removido com sucesso."));
            }
            catch (System.Exception ex)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message));

            }
        }
    }
}
