﻿using Am.Clientes.Api.App_Start;
using Am.Clientes.Application.AutoMapper;
using Am.Clientes.Application.Interfaces;
using Am.Clientes.Application.Services;
using Am.Clientes.Domain.Interfaces.Repositories;
using Am.Clientes.Domain.Interfaces.Services;
using Am.Clientes.Domain.Services;
using Am.Clientes.InfraStructure.Repositories;

using System.Web.Http;
using Unity;
using Unity.Lifetime;

namespace Am.Clientes.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            container.RegisterType(typeof(IServiceBase<>), typeof(ServiceBase<>));
            container.RegisterType(typeof(IRepositoryBase<>), typeof(RepositoryBase<>));

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IClienteAppService, ClienteAppService>();
            container.RegisterType<IEnderecoAppService, EnderecoAppService>();

            //Service
            container.RegisterType<IClienteService, ClienteService>();
            container.RegisterType<IEnderecoService, EnderecoService>();
            //Repository
            container.RegisterType<IClienteRepository, ClienteRepository>();
            container.RegisterType<IEnderecoRepository, EnderecoRepository>();

            config.DependencyResolver = new UnityResolver(container);


            AutoMapperConfiguration.Configuration();

           
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
