﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.AutoMapper
{
   public class AutoMapperConfiguration
    {
        public static void Configuration()
        {
            Mapper.Initialize(x => {
                x.AddProfile<DomainToVmMappingProfile>();
                x.AddProfile<VmToDomainMappingProlfile>();

            });
        }
    }
}
