﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Domain.Entities;
using AutoMapper;

namespace Am.Clientes.Application.AutoMapper
{
    internal class DomainToVmMappingProfile : Profile
    {
        public DomainToVmMappingProfile()
        {
            CreateMap<Cliente,ClienteDTO>();
            CreateMap<Endereco,EnderecoDTO>();
            

        }
    }
}