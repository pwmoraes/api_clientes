﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Domain.Entities;
using AutoMapper;

namespace Am.Clientes.Application.AutoMapper
{
    internal class VmToDomainMappingProlfile : Profile
    {
        public VmToDomainMappingProlfile()
        {
            CreateMap<ClienteDTO, Cliente>();
            CreateMap<EnderecoDTO, Endereco>();
           
        }
    }
}