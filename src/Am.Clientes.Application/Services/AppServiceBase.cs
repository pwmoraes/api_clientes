﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Application.Interfaces;
using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Services;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.Services
{
    

    public class AppServiceBase<TEntity, TEntityDTO> : IAppServiceBase<TEntity, TEntityDTO>, IDisposable
        where TEntity : EntityBase where TEntityDTO : EntityBaseDTO
    {

        //DI
        protected readonly IServiceBase<TEntity> _service;

        public AppServiceBase(IServiceBase<TEntity> service)
        {
            _service = service;


        }

        public TEntityDTO Add(TEntityDTO entity)
        {
            try
            {
                //Mapeia o DTO para Entity
                TEntity _entity = Mapper.Map<TEntityDTO,TEntity>(entity);
                //Realiza a inserção já mapeando o retorno para DTO
                return Mapper.Map<TEntity, TEntityDTO>(_service.Add(_entity));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TEntityDTO Edit(TEntityDTO entity)
        {
            try
            {
                //Mapeia o DTO para Entity
                TEntity _entity = Mapper.Map<TEntity>(entity);
                //Realiza a edição já mapeando o retorno para DTO
                return Mapper.Map<TEntity, TEntityDTO>(_service.Edit(_entity));
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Remove(int id)
        {
            try
            {
                _service.Remove(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TEntityDTO Get(int id)
        {
            try
            {
                //Realiza a consulta já mapeando o retorno para DTO
                return Mapper.Map<TEntity, TEntityDTO>(_service.Get(id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<TEntityDTO> Get(Expression<Func<TEntityDTO, bool>> predicate = null)
        {
            try
            {
                //Mapeia o DTO para Entity
                Expression<Func<TEntity, bool>> _predicate = Mapper.Map<Expression<Func<TEntity, bool>>>(predicate);
                //Realiza a consulta já mapeando o retorno para DTO
                return Mapper.Map<IEnumerable<TEntity>, IEnumerable<TEntityDTO>>(_service.Get(_predicate));


            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Dispose()
        {
            _service.Dispose();
        }
    }


}
