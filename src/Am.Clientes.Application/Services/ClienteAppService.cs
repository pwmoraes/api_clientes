﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Application.Interfaces;
using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Services;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.Services
{
    public class ClienteAppService : AppServiceBase<Cliente, ClienteDTO>, IClienteAppService
    {

        private readonly IClienteService _clienteService;
        public ClienteAppService(IServiceBase<Cliente> service, IClienteService clienteService) : base(service)
        {
            _clienteService = clienteService;
        }

        public ClienteDTO AdicionarCliente(ClienteDTO cliente)
        {
            return Mapper.Map<ClienteDTO>(_clienteService.AdicionarCliente(Mapper.Map<Cliente>(cliente)));
        }

        public ClienteDTO EditarCliente(ClienteDTO cliente)
        {
            return Mapper.Map<ClienteDTO>(_clienteService.EditarCliente(Mapper.Map<Cliente>(cliente)));
        }
    }
}
