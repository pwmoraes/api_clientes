﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Application.Interfaces;
using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Services;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.Services
{
    public class EnderecoAppService : AppServiceBase<Endereco, EnderecoDTO>, IEnderecoAppService
    {

        private readonly IEnderecoService _enderecoService;
        public EnderecoAppService(IServiceBase<Endereco> service, IEnderecoService enderecoService) : base(service)
        {
            _enderecoService = enderecoService;
        }

        

        public EnderecoDTO AdicionarEndereco(EnderecoDTO endereco)
        {
            return Mapper.Map<EnderecoDTO>(_enderecoService.AdicionarEndereco(Mapper.Map<Endereco>(endereco)));

        }

        public EnderecoDTO EditarEndereco(EnderecoDTO endereco)
        {
            return Mapper.Map<EnderecoDTO>(_enderecoService.EditarEndereco(Mapper.Map<Endereco>(endereco)));

        }
    }
}
