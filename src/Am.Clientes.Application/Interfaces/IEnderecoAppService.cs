﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.Interfaces
{
    public interface IEnderecoAppService : IAppServiceBase<Endereco,EnderecoDTO>
    {
        EnderecoDTO AdicionarEndereco(EnderecoDTO endereco);
        EnderecoDTO EditarEndereco(EnderecoDTO endereco);
    }
}
