﻿using Am.Clientes.Application.DTO;
using Am.Clientes.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.Interfaces
{
    //public interface IAppServiceBase<TEntity> : IDisposable
    //    where TEntity : EntityBase 
    //{
    //    TEntity Add(TEntity entity);
    //    TEntity Get(int id);
    //    IEnumerable<TEntity> Get(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null);
    //    TEntity Edit(TEntity entity);
    //    void Remove(int id);
    //}

    public interface IAppServiceBase<TEntity, TEntityDTO> : IDisposable
        where TEntity : EntityBase where TEntityDTO : EntityBaseDTO
    {
        TEntityDTO Add(TEntityDTO entity);
        TEntityDTO Get(int id);
        IEnumerable<TEntityDTO> Get(System.Linq.Expressions.Expression<Func<TEntityDTO, bool>> predicate = null);
        TEntityDTO Edit(TEntityDTO entity);
        void Remove(int id);
    }


}
