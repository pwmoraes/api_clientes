﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.DTO
{
    public class ClienteDTO : EntityBaseDTO
    {
        [Required]
        [MaxLength(30)]
        public string Nome { get; set; }
        [Required]
        public string Cpf { get; set; }
        [Required]
        public DateTime DataNascimento { get; set; }
        public int Idade { get; set; }

        [Required]
        public EnderecoDTO Endereco { get; set; }
    }
}
