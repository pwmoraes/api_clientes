﻿using System.ComponentModel.DataAnnotations;

namespace Am.Clientes.Application.DTO
{
    public abstract class EntityBaseDTO
    {
        [Key]
        public int Id { get; set; }
    }
}
