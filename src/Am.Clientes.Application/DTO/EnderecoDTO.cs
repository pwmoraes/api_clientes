﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.Application.DTO
{
    public class EnderecoDTO : EntityBaseDTO
    {
        [Required]
        [MaxLength(50)]
        public string Logradouro { get;  set; }
        [MaxLength(10)]
        public string Numero { get;  set; }
        [MaxLength(10)]
        public string Complemento { get;  set; }
        [Required]
        [MaxLength(40)]
        public string Bairro { get;  set; }
        [Required]
        [MaxLength(40)]
        public string Cidade { get;  set; }
        [Required]
        [MaxLength(40)]
        public string Estado { get;  set; }

        public EnderecoDTO()
        {

        }
    }
}
