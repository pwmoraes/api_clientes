﻿using Am.Clientes.Domain.Entities;
using Am.Clientes.InfraStructure.EntityConfig;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Am.Clientes.InfraStructure.Context
{
    public class ClienteContext : DbContext
    {
        public ClienteContext() : base("ClienteContextLocal")
        {

        }

        public DbSet<Cliente> Cliente { get; set; }
        public DbSet<Endereco> Endereco { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Remove a convenção que pluraliza os nomes das tabelas
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //Remove a remoção em cascata
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            //Define todos os campos 'string' como 'varchar' e ,caso não seja informado, define o tamanho como 45
            modelBuilder.Properties<string>().Configure(x => x.HasColumnType("varchar").HasMaxLength(45));

            //Entities Configurations 
            modelBuilder.Configurations.Add(new ClienteConfiguration());
            modelBuilder.Configurations.Add(new EnderecoConfiguration());
            
        }

       

    }
}
