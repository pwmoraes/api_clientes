﻿using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Repositories;
using Am.Clientes.InfraStructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.InfraStructure.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : EntityBase
    {        
        protected readonly ClienteContext _context = new ClienteContext();
       
        public TEntity Add(TEntity entity)
        {
            try
            {
                _context.Set<TEntity>().Add(entity);
                _context.SaveChanges();
                return entity;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
        public TEntity Edit(TEntity entity)
        {
            try
            {
                _context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();
                return entity;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public TEntity Get(int id)
        {
            try
            {
               return _context.Set<TEntity>().Where(x => x.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null)
        {
            try
            {
                if(predicate != null)
                    return _context.Set<TEntity>().Where(predicate).AsEnumerable();
                
                var rst = _context.Set<TEntity>().AsEnumerable();
                return rst;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Remove(int id)
        {
            try
            {
                _context.Set<TEntity>().Remove(Get(id));
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            if (_context != null)
                _context.Dispose();
        }

    }
}
