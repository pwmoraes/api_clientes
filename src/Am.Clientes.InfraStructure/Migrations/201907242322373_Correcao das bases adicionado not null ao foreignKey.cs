namespace Am.Clientes.InfraStructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrecaodasbasesadicionadonotnullaoforeignKey : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Cliente", new[] { "Endereco_Id" });
            AlterColumn("dbo.Cliente", "Endereco_Id", c => c.Int(nullable: false));
            CreateIndex("dbo.Cliente", "Endereco_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Cliente", new[] { "Endereco_Id" });
            AlterColumn("dbo.Cliente", "Endereco_Id", c => c.Int());
            CreateIndex("dbo.Cliente", "Endereco_Id");
        }
    }
}
