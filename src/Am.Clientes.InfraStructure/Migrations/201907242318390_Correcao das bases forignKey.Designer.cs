// <auto-generated />
namespace Am.Clientes.InfraStructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CorrecaodasbasesforignKey : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CorrecaodasbasesforignKey));
        
        string IMigrationMetadata.Id
        {
            get { return "201907242318390_Correcao das bases forignKey"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
