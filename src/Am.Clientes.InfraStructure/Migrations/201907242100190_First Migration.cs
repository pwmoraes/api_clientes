namespace Am.Clientes.InfraStructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EntityBase",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 30, unicode: false),
                        Cpf = c.String(maxLength: 45, unicode: false),
                        DataNascimento = c.DateTime(),
                        Idade = c.Int(),
                        Logradouro = c.String(maxLength: 50, unicode: false),
                        Numero = c.String(maxLength: 10, unicode: false),
                        Complemento = c.String(maxLength: 10, unicode: false),
                        Bairro = c.String(maxLength: 40, unicode: false),
                        Cidade = c.String(maxLength: 40, unicode: false),
                        Estado = c.String(maxLength: 40, unicode: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EntityBase");
        }
    }
}
