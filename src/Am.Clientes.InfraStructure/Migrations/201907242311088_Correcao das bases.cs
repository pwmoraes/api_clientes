namespace Am.Clientes.InfraStructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Correcaodasbases : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.EntityBase", newName: "Cliente");
            CreateTable(
                "dbo.Endereco",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Logradouro = c.String(nullable: false, maxLength: 50, unicode: false),
                        Numero = c.String(maxLength: 10, unicode: false),
                        Complemento = c.String(maxLength: 10, unicode: false),
                        Bairro = c.String(nullable: false, maxLength: 40, unicode: false),
                        Cidade = c.String(nullable: false, maxLength: 40, unicode: false),
                        Estado = c.String(nullable: false, maxLength: 40, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.Cliente", "Nome", c => c.String(nullable: false, maxLength: 30, unicode: false));
            AlterColumn("dbo.Cliente", "Cpf", c => c.String(nullable: false, maxLength: 45, unicode: false));
            AlterColumn("dbo.Cliente", "DataNascimento", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Cliente", "Idade", c => c.Int(nullable: false));
            DropColumn("dbo.Cliente", "Logradouro");
            DropColumn("dbo.Cliente", "Numero");
            DropColumn("dbo.Cliente", "Complemento");
            DropColumn("dbo.Cliente", "Bairro");
            DropColumn("dbo.Cliente", "Cidade");
            DropColumn("dbo.Cliente", "Estado");
            DropColumn("dbo.Cliente", "Discriminator");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cliente", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Cliente", "Estado", c => c.String(maxLength: 40, unicode: false));
            AddColumn("dbo.Cliente", "Cidade", c => c.String(maxLength: 40, unicode: false));
            AddColumn("dbo.Cliente", "Bairro", c => c.String(maxLength: 40, unicode: false));
            AddColumn("dbo.Cliente", "Complemento", c => c.String(maxLength: 10, unicode: false));
            AddColumn("dbo.Cliente", "Numero", c => c.String(maxLength: 10, unicode: false));
            AddColumn("dbo.Cliente", "Logradouro", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Cliente", "Idade", c => c.Int());
            AlterColumn("dbo.Cliente", "DataNascimento", c => c.DateTime());
            AlterColumn("dbo.Cliente", "Cpf", c => c.String(maxLength: 45, unicode: false));
            AlterColumn("dbo.Cliente", "Nome", c => c.String(maxLength: 30, unicode: false));
            DropTable("dbo.Endereco");
            RenameTable(name: "dbo.Cliente", newName: "EntityBase");
        }
    }
}
