namespace Am.Clientes.InfraStructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CorrecaodasbasesforignKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cliente", "Endereco_Id", c => c.Int());
            CreateIndex("dbo.Cliente", "Endereco_Id");
            AddForeignKey("dbo.Cliente", "Endereco_Id", "dbo.Endereco", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cliente", "Endereco_Id", "dbo.Endereco");
            DropIndex("dbo.Cliente", new[] { "Endereco_Id" });
            DropColumn("dbo.Cliente", "Endereco_Id");
        }
    }
}
