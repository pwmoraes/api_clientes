﻿using Am.Clientes.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.InfraStructure.EntityConfig
{
    public class EnderecoConfiguration : EntityTypeConfiguration<Endereco>
    {
        public EnderecoConfiguration()
        {
            HasKey(x => x.Id);

            Property(x => x.Logradouro).IsRequired().HasMaxLength(50);
            Property(x => x.Numero).HasMaxLength(10);
            Property(x => x.Complemento).HasMaxLength(10);
            Property(x => x.Bairro).IsRequired().HasMaxLength(40);
            Property(x => x.Cidade).IsRequired().HasMaxLength(40);
            Property(x => x.Estado).IsRequired().HasMaxLength(40);


        }
    }
}
