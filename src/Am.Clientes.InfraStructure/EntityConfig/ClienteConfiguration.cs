﻿using Am.Clientes.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Am.Clientes.InfraStructure.EntityConfig
{
   public class ClienteConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClienteConfiguration()
        {
            HasKey(x => x.Id);
            Property(x => x.Nome).IsRequired().HasMaxLength(30);
            Property(x => x.Cpf).IsRequired();
            Property(x => x.DataNascimento).IsRequired();

           // HasRequired(x=>x.)
        }
    }
}
