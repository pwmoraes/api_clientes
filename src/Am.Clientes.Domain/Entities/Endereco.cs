﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Am.Clientes.Domain.Entities
{
    public class Endereco : EntityBase
    {
       
        [Required]
        [MaxLength(50)]
        public string Logradouro { get; private set; }
        [MaxLength(10)]
        public string Numero { get; private set; }
        [MaxLength(10)]
        public string Complemento { get; private set; }
        [Required]
        [MaxLength(40)]
        public string Bairro { get; private set; }
        [Required]
        [MaxLength(40)]
        public string Cidade { get; private set; }
        [Required]
        [MaxLength(40)]
        public string Estado { get; private set; }

        public Endereco()
        {

        }

        public Endereco(Endereco endereco)
        {
            //Validar Endereço
            ValidarEndereco(endereco);

            //objeto válido
            Logradouro = endereco.Logradouro;
            Numero = endereco.Numero;
            Complemento = endereco.Complemento;
            Bairro = endereco.Bairro;
            Cidade = endereco.Cidade;
            Estado = endereco.Estado;
        }

        private void ValidarEndereco(Endereco endereco)
        {
            if (string.IsNullOrEmpty(endereco.Logradouro))
                throw new ArgumentException("Logradouro é inválido");
            if (endereco.Logradouro.Length > 50)
                throw new ArgumentException("Logradouro é inválido");

            if (string.IsNullOrEmpty(endereco.Bairro))
                throw new ArgumentException("Bairro é inválido");
            if (endereco.Bairro.Length > 40)
                throw new ArgumentException("Bairro é inválido");

            if (string.IsNullOrEmpty(endereco.Cidade))
                throw new ArgumentException("Cidade é inválida");
            if (endereco.Cidade.Length > 40)
                throw new ArgumentException("Cidade é inválida");

            if (string.IsNullOrEmpty(endereco.Estado))
                throw new ArgumentException("Estado é inválido");
            if (endereco.Estado.Length > 40)
                throw new ArgumentException("Estado é inválido");
            
        }

    }
}
