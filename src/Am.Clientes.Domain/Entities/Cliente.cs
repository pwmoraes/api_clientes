﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Am.Clientes.Domain.Entities
{
    public class Cliente : EntityBase
    {
        
        [Required]
        [MaxLength(30)]
        public string Nome { get;  set; }
        [Required]
        public string Cpf { get;  set; }
        [Required]
        public DateTime DataNascimento { get;  set; }
        public int Idade { get;  set; }

        [Required]
        public virtual Endereco Endereco { get; set; }

        //Construtor da classe
        public Cliente(Cliente cliente)
        {
            //valida as regras de negócio
            ValidarCliente(cliente);

            //Campos válidos
            cliente.Idade = CalcularIdade(cliente.DataNascimento);

        }

        public Cliente()
        {

        }

        /// <summary>
        /// Realiza a validação do Cliente baseado nas regras fornecidas
        /// </summary>
        /// <param name="cliente">(Cliente) Objeto Cliente</param>
        private void ValidarCliente(Cliente cliente)
        {
            if (string.IsNullOrEmpty(cliente.Nome))
                throw new ArgumentException("Nome do cliente é inválido");

            if (cliente.Nome.Length > 30)
                throw new ArgumentException("Nome do cliente é inválido");

            if (string.IsNullOrEmpty(cliente.Cpf))
                throw new ArgumentException("Cpf do cliente é inválido");

            if (!ValidarCpf(cliente.Cpf))
                throw new ArgumentException("Cpf do cliente é inválido");

            if (cliente.DataNascimento == null)
                throw new ArgumentException("Data de nascimento do cliente é inválida");

             if (cliente.DataNascimento == DateTime.Parse("01/01/0001"))
                throw new ArgumentException("Data de nascimento do cliente é inválida");

            


        }

        /// <summary>
        /// Realiza o cálculo da idade do cliente baseado na sua data de nascimento
        /// </summary>
        /// <param name="dataNascimento">Data de nascimento do cliente</param>
        /// <returns>(int) Idade</returns>
        private int CalcularIdade(DateTime dataNascimento)
        {
            var idade = DateTime.Today.Year - dataNascimento.Year;
            if (dataNascimento > DateTime.Today.AddYears(-idade))
                idade--;
            return idade;
        }

        /// <summary>
        /// Realiza a validação do núremo de cpf do cliente
        /// </summary>
        /// <param name="numeroCpf"></param>
        /// <returns></returns>
        private bool ValidarCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;
            cpf = cpf.Trim();
            cpf = cpf.Replace(".", "").Replace("-", "");
            if (cpf.Length != 11)
                return false;
            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);

        }

    }
}
