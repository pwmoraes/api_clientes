﻿using Am.Clientes.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Am.Clientes.Domain.Interfaces.Services
{
    public interface IEnderecoService : IServiceBase<Endereco>
    {
        Endereco AdicionarEndereco(Endereco endereco);
        Endereco EditarEndereco(Endereco endereco);
    }
}
