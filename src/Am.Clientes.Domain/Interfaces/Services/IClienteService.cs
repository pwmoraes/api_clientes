﻿using Am.Clientes.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Am.Clientes.Domain.Interfaces.Services
{
    public interface IClienteService : IServiceBase<Cliente>
    {
        Cliente AdicionarCliente(Cliente cliente);
        Cliente EditarCliente(Cliente cliente);

    }
}
