﻿using Am.Clientes.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Am.Clientes.Domain.Interfaces.Repositories
{
    public interface IRepositoryBase<TEntity> : IDisposable where TEntity : EntityBase
    {
        TEntity Add(TEntity entity);
        TEntity Get(int id);
        IEnumerable<TEntity> Get(System.Linq.Expressions.Expression<Func<TEntity, bool>> predicate = null);
        TEntity Edit(TEntity entity);
        void Remove(int id);
    }
}
