﻿using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Repositories;
using Am.Clientes.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Am.Clientes.Domain.Services
{
    public class EnderecoService : ServiceBase<Endereco>, IEnderecoService
    {
        public EnderecoService(IRepositoryBase<Endereco> repository) : base(repository)
        {
        }

        public Endereco AdicionarEndereco(Endereco endereco)
        {
            new Endereco(endereco);
            return Add(endereco);
        }

        public Endereco EditarEndereco(Endereco endereco)
        {
            new Endereco(endereco);
            return Edit(endereco);
        }
    }
}
