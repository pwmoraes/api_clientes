﻿using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Repositories;
using Am.Clientes.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Am.Clientes.Domain.Services
{
    public class ServiceBase<TEntity> : IServiceBase<TEntity> where TEntity : EntityBase
    {
        //DI
        private readonly IRepositoryBase<TEntity> _repository;
        public ServiceBase(IRepositoryBase<TEntity> repository)
        {
            _repository = repository;
        }

        public TEntity Add(TEntity entity)
        {
            try
            {
                return _repository.Add(entity);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TEntity Edit(TEntity entity)
        {
            try
            {
                return _repository.Edit(entity);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Remove(int id)
        {
            try
            {
                _repository.Remove(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public TEntity Get(int id)
        {
            try
            {
                return _repository.Get(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> predicate = null)
        {
            try
            {
                return _repository.Get(predicate);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Dispose()
        {
            _repository.Dispose();
        }
    }
}
