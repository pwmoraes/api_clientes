﻿using Am.Clientes.Domain.Entities;
using Am.Clientes.Domain.Interfaces.Repositories;
using Am.Clientes.Domain.Interfaces.Services;

namespace Am.Clientes.Domain.Services
{
    public class ClienteService : ServiceBase<Cliente>, IClienteService
    {
        public ClienteService(IRepositoryBase<Cliente> repository) : base(repository)
        {
        }

        public Cliente AdicionarCliente(Cliente cliente)
        {
            //Valida o cliente
            new Cliente(cliente);
            return Add(cliente);
        }

        public Cliente EditarCliente(Cliente cliente)
        {
            new Cliente(cliente);
            return Edit(cliente);
        }
    }
}
